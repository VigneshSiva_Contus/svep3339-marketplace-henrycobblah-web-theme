<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Slydepay
 * @package     Slydepay
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Slydepay_ProcessingController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
	//global $_COOKIE;
	protected $_servUri = 'https://app.slydepay.com.gh/webservices/paymentservice.asmx?wsdl';
	protected $_payUri = 'https://app.slydepay.com.gh/paylive/detailsnew.aspx';
	protected $_ns = 'http://www.i-walletlive.com/payLIVE';
	
	protected function _setHeader($soapClient){
		// header data
		$APIVersion = 1.4;
		$MerchantKey = Mage::getStoreConfig('slydepay/settings/secret_key');
		$MerchantEmail = Mage::getStoreConfig('slydepay/settings/slydepay_email');
		$SvcType = 'C2B';
		$UseIntMode = Mage::getStoreConfig('slydepay/settings/active');
		
		$header_params = array(
			"APIVersion"	=> trim($APIVersion),
			"MerchantKey"	=> trim($MerchantKey),
			"MerchantEmail" => trim($MerchantEmail),
			"SvcType" 		=> $SvcType,
			"UseIntMode" 	=> $UseIntMode,
		);
		
		$header = new SOAPHeader($this->_ns, 'PaymentHeader', $header_params);
		$soapClient->__setSoapHeaders($header);
		
		return TRUE;
	}
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Iframe page which submits the payment data to Slydepay.
     */
    public function placeformAction()
    {
	   $this->loadLayout();
       $this->renderLayout();
    }

    /**
     * Show orderPlaceRedirect page which contains the Slydepay iframe.
     */
    public function paymentAction()
    {
				
		try {
            $session = $this->_getCheckout();
			$order = Mage::getModel('sales/order');
			$order->loadByIncrementId($session->getLastRealOrderId());
			$products = array();       
			$resposnse = array();
			$resposnse['error'] = '';
			$resposnse['payToken'] = '';
			$comment = '';
			// create soap instance
			$soapClient = new SoapClient($this->_servUri);
			// set header
			$this->_setHeader($soapClient);	
			foreach($order->getAllItems() as $item){
				$item = $item->toArray();           
				$products[] = array(
					'ItemCode' => $item['sku'],
					'ItemName' => $item['name'],
					'UnitPrice' => number_format($item['price'],2,'.',''),
					'Quantity' =>  (int)$item['qty_ordered'],
					'SubTotal' =>  number_format($item['price']*$item['qty_ordered'],2,'.','')
				);					
				$comment .= (int)$item['qty_ordered'].' '.$item['name'].', ';
			}
			//slydepay vars to send        
			$params = array();
			//$params['orderId'] = $order->getId();
			$params['orderId'] = $order->getRealOrderId();
			$params['subtotal'] = $order->getSubtotal();
			$params['shippingCost'] = $order->getShippingAmount();
			$params['taxAmount'] = $order->getTaxAmount();
			$params['total'] = round($order->getGrandTotal(), 2);
			$params['comment1'] = substr($comment, 0, -2);;
			$params['orderItems'] = array('OrderItem' => $products);

		
			//make a request for proccessing payment
			$result = $soapClient->ProcessPaymentOrder($params);
			// check for the request (for debug only)
			
			//echo $soapClient->__getLastRequest();exit;
			// if soap fault occerred
			if (is_soap_fault($result)) {
				$resposnse['error'] = 'ERROR: SOAP Fault! Order proccessing failed!';
			}
			// check response and do accordingly
			if(isset($result->ProcessPaymentOrderResult) && strlen($result->ProcessPaymentOrderResult)==36){
				$resposnse['payToken'] = $result->ProcessPaymentOrderResult;
				$resposnse['order_id'] = $order->getId();
				$resposnse['pay_uri'] = $this->_payUri;
			}elseif(isset($result->ProcessPaymentOrderResult)){
				$resposnse['error'] = $result->ProcessPaymentOrderResult;
			}else{
				$resposnse['error'] = 'Unknow ERROR! Order proccessing failed!';
			}
			if($resposnse['error']!='')
			{
				$this->_getCheckout()->addError($resposnse['error']);
				 parent::_redirect('checkout/cart');
			}
			
			Mage::register('r_url', $resposnse['pay_uri']."?pay_token=".$resposnse['payToken']."&provider=".$_COOKIE['iw_provider']);// In the Controller
			
           
            if (!$order->getId()) {
                $this->_getCheckout()->addError('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('slydepay')->__('The customer was redirected to Slydepay.')
            );
            $order->save();

            $session->setSlydepayQuoteId($session->getQuoteId());
            $session->setSlydepayRealOrderId($session->getLastRealOrderId());
            $session->getQuote()->setIsActive(false)->save();
            $session->clear();

            $this->loadLayout();
            $this->renderLayout();
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    }

    /**
     * Action to which the customer will be returned when the payment is made.
     */
    public function successAction()
    {
		$respns=$this->getRequest()->getParams();
		
		$status = isset($respns['status']) ? $respns['status'] : -1;
		$cust_ref = isset($respns['cust_ref']) ? $respns['cust_ref'] : '';
		$transac_id = isset($respns['transac_id']) ? $respns['transac_id'] : '';
		$pay_token = isset($respns['pay_token']) ? $respns['pay_token'] : '';
		
		
		if($status == 0){ // Transaction successful and approved
		// create soap instance
		$soapClient = new SoapClient($this->_servUri);
		// set header
		$this->_setHeader($soapClient);
		// set params
		$params = array();
		$params['payToken'] = $pay_token;
		$params['transactionId'] = $transac_id;
		// make a request to confirm transaction
		$result = $soapClient->ConfirmTransaction($params);

		if(isset($result->ConfirmTransactionResult) && $result->ConfirmTransactionResult==1){
		//Confirmation Successful
			$this->statusAction();
			$event = Mage::getModel('slydepay/event')
			->setEventData($this->getRequest()->getParams());
			try {	
				//$event->processStatusEvent();
				$quoteId = $event->successEvent();			
				$this->_getCheckout()->setLastSuccessQuoteId($quoteId);
				$this->_redirect('checkout/onepage/success');
				return;
			} catch (Mage_Core_Exception $e) {
				$this->_getCheckout()->addError($e->getMessage());
			} catch(Exception $e) {
				Mage::logException($e);
			}
			$this->_redirect('checkout/cart');
			
		}elseif(isset($result->ConfirmTransactionResult) && $result->ConfirmTransactionResult==0){ //Confirmation failed: Invalid transaction Id
			$this->_getCheckout()->addError('Confirmation failed: Invalid transaction Id');
			//$this->redirect(HTTPS_SERVER . 'index.php?route=checkout/confirm');
		}elseif(isset($result->ConfirmTransactionResult) && $result->ConfirmTransactionResult==-1){ //Confirmation Failed: Invalid pay token
			$this->_getCheckout()->addError('Confirmation Failed: Invalid pay token');
			//$this->redirect(HTTPS_SERVER . 'index.php?route=checkout/confirm');
		}else{
			$this->_redirect('checkout/cart');
		}
		}elseif($status==-1){ // Technical error contact
			$this->_getCheckout()->addError('Technical error contact');
		}elseif($status==-2){ // User cancelled transaction
			$this->cancelAction();
			//$this->_redirect('checkout/cart');
		}
				
    }

    /**
     * Action to which the customer will be returned if the payment process is
     * cancelled.
     * Cancel order and redirect user to the shopping cart.
     */
    public function cancelAction()
    {
        $event = Mage::getModel('slydepay/event')
                 ->setEventData($this->getRequest()->getParams());
        $message = $event->cancelEvent();

        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getSlydepayQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }

        $session->addError($message);
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the transaction details will be posted after the payment
     * process is complete.
     */
    public function statusAction()
    {
        $event = Mage::getModel('slydepay/event')
            ->setEventData($this->getRequest()->getParams());
        $message = $event->processStatusEvent();
        $this->getResponse()->setBody($message);
    }

    /**
     * Set redirect into responce. This has to be encapsulated in an JavaScript
     * call to jump out of the iframe.
     *
     * @param string $path
     * @param array $arguments
     */
    protected function _redirect($path, $arguments=array())
    {
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('slydepay/redirect')
                ->setRedirectUrl(Mage::getUrl($path, $arguments))
                ->toHtml()
        );
        return $this;
    }
}
