<?php
/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Marketplace
 * @version     1.8.0
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 * 
 */
/**
 * Function written in this file are globally accessed
 */
class Apptha_Marketplace_Helper_Assign extends Mage_Core_Helper_Abstract {
	
	/**
	 * Assign configurable product attribute values
	 */
	public function setconfigurableProductDataForAssignProduct($configurableAttributes,$newProduct,$childProductData,$childProductId){
		$productConfigData = array();
		 
		if (count ( $configurableAttributes ) >= 1) {
			$productConfigData = array ();
			/**
			 * Adding data to product instanse
			*/
			foreach ( $configurableAttributes as $configurableAttribute ) {
				if (! isset( $childProductData[$childProductId][$configurableAttribute] )) {
					$productAttributeOption = Mage::getModel ( 'catalog/product' );
					$attr = $productAttributeOption->getResource ()->getAttribute ( $configurableAttribute );
					$optionValueFoConfig = $attr->getSource ()->getOptionId ($childProductData[$childProductId][$configurableAttribute]);
					$productConfigData [$configurableAttribute] = $optionValueFoConfig;
				}
			}
			if (! empty ( $productConfigData )) {
				$newProduct->addData ( $productConfigData );
			}
		}
		return $newProduct;
	}
    
}