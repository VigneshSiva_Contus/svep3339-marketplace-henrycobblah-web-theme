<?php
/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Marketplace
 * @version     1.8.0
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 * 
 */

?>
<?php
/**
 *  This class contains banner slider save and edit action
 */ 
class Apptha_Bannerslider_Adminhtml_BannersliderController extends Mage_Adminhtml_Controller_Action{
	/**
	 * Init action for banner slider
	 */
	protected function _initAction() {
		$this->loadLayout ()->_setActiveMenu ( 'marketplace/items' )->_addBreadcrumb ( Mage::helper ( 'adminhtml' )->__ ( 'Banner Sliders' ), Mage::helper ( 'adminhtml' )->__ ( 'Banner Sliders' ) );
		return $this;
	}
	
	/**
	 * To set title and layout for banner slider
	 */
	public function indexAction() {
		$this->_title ( $this->__ ( 'Banner slider' ) );
		$this->_initAction ()->renderLayout ();
	}
	
	/**
	 * New action forward to exit funcation
	 */
	public function newAction() {
		$this->_forward ( 'edit' );
	}
	
	/**
	 * Edit action for banner slider
	 */
	public function editAction() {
		$id = $this->getRequest ()->getParam ( 'id' );
		$bannerModel = Mage::getModel ( 'bannerslider/bannerslider' )->load ( $id );
		
		if ($bannerModel->getId () || $id == 0) {
			$post = Mage::getSingleton ( 'adminhtml/session' )->getFormData ( true );
			if (! empty ( $post )) {
				$bannerModel->setData ( $post );
			}
			
			Mage::register ( 'bannerslider_data', $bannerModel );
			/**
			 * Setting for grid title
			 */
			$this->_title ( $this->__ ( 'Bannerslider' ) )->_title ( $this->__ ( 'Manage banner' ) );
			if ($bannerModel->getId ()) {
				$this->_title ( $bannerModel->getTitle () );
			} else {
				$this->_title ( $this->__ ( 'New Banner' ) );
			}
			
			/**
			 * Set active button for marketplace
			 */
			$this->loadLayout ();
			$this->_setActiveMenu ( 'marketplace/items' );
			
			/**
			 * Set breadcrumb
			 */
			$this->_addBreadcrumb ( Mage::helper ( 'adminhtml' )->__ ( 'Item Manager' ), Mage::helper ( 'adminhtml' )->__ ( 'Item Manager' ) );
			$this->_addBreadcrumb ( Mage::helper ( 'adminhtml' )->__ ( 'Item News' ), Mage::helper ( 'adminhtml' )->__ ( 'Item News' ) );
			
			$this->getLayout ()->getBlock ( 'head' )->setCanLoadExtJs ( true );
			
			$this->_addContent ( $this->getLayout ()->createBlock ( 'bannerslider/adminhtml_bannerslider_edit' ) )->_addLeft ( $this->getLayout ()->createBlock ( 'bannerslider/adminhtml_bannerslider_edit_tabs' ) );
			
			$this->renderLayout ();
		} else {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'bannerslider' )->__ ( 'Item does not exist' ) );
			$this->_redirect ( '*/*/' );
		}
	}
	
	/**
	 * Save banner slider funcation
	 */
	public function saveAction() {
		if ($post = $this->getRequest ()->getPost ()) {
			
			if ($post ['file'] ['delete'] == 1) {
				$post ['file'] = '';
			} elseif (is_array ( $post ['file'] )) {
				$post ['file'] = $post ['file'] ['value'];
			}
			
			$file = new Varien_Io_File ();
			$imageDir = Mage::getBaseDir ( 'media' ) . DS . 'sliderimages';
			$thumbimageyDir = Mage::getBaseDir ( 'media' ) . DS . 'sliderimages' . DS . 'thumbs';
			
			if (! is_dir ( $imageDir )) {
				$imageDirResult = $file->mkdir ( $imageDir, 0777 );
			}
			if (! is_dir ( $thumbimageyDir )) {
				$thumbimageDirResult = $file->mkdir ( $thumbimageyDir, 0777 );
			}
			
			if (isset ( $_FILES ['file'] ['name'] ) && $_FILES ['file'] ['name'] != '') {
				try {
					$uploaderObj = new Varien_File_Uploader ( 'file' );
					
					$uploaderObj->setAllowedExtensions ( array (
							'jpg',
							'jpeg',
							'gif',
							'png' 
					) );
					$uploaderObj->setAllowRenameFiles ( true );
					
					$uploaderObj->setFilesDispersion ( true );
					
					$path = $imageDir . DS;
					$result = $uploaderObj->save ( $path, $_FILES ['file'] ['name'] );
					$file = str_replace ( DS, '/', $result ['file'] );
					
					$imageUrl = Mage::getBaseDir ( 'media' ) . DS . "sliderimages" . $file;
					
					$imageResized = Mage::getBaseDir ( 'media' ) . DS . "sliderimages" . DS . "thumbs" . DS . "sliderimages" . $file;
					/**
					 * Save slider images
					 */
					if (! file_exists ( $imageResized ) && file_exists ( $imageUrl )) :
						$imageObj = new Varien_Image ( $imageUrl );
						$imageObj->constrainOnly ( TRUE );
						$imageObj->keepAspectRatio ( FALSE );
						$imageObj->keepFrame ( FALSE );
						$imageObj->quality ( 100 );
						$imageObj->resize ( 80, 50 );
						$imageObj->save ( $imageResized );
					
					endif;
					
					$post ['file'] = 'sliderimages' . $file;
				} catch ( Exception $e ) {
					$post ['file'] = 'sliderimages' . '/' . $_FILES ['file'] ['name'];
				}
			}
			
			/**
			 * Save slider data
			 */
			$bannerModel = Mage::getModel ( 'bannerslider/bannerslider' );
			$bannerModel->setData ( $post )->setId ( $this->getRequest ()->getParam ( 'id' ) );
			
			try {
				if ($bannerModel->getCreatedTime == NULL || $bannerModel->getUpdateTime () == NULL) {
					$bannerModel->setCreatedTime ( date ( "Y-m-d H:i:s", Mage::getModel ( 'core/date' )->timestamp ( time () ) ) )->setUpdateTime ( date ( "Y-m-d H:i:s", Mage::getModel ( 'core/date' )->timestamp ( time () ) ) );
				} else {
					$bannerModel->setUpdateTime ( date ( "Y-m-d H:i:s", Mage::getModel ( 'core/date' )->timestamp ( time () ) ) );
				}
				
				$bannerModel->save ();
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'bannerslider' )->__ ( 'Slider Image have been saved successfully' ) );
				Mage::getSingleton ( 'adminhtml/session' )->setFormData ( false );
				
				if ($this->getRequest ()->getParam ( 'back' )) {
					$this->_redirect ( '*/*/edit', array (
							'id' => $bannerModel->getId () 
					) );
					return;
				}
				$this->_redirect ( '*/*/' );
				return;
			} catch ( Exception $e ) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
				Mage::getSingleton ( 'adminhtml/session' )->setFormData ( $post );
				$this->_redirect ( '*/*/edit', array (
						'id' => $this->getRequest ()->getParam ( 'id' ) 
				) );
				return;
			}
		}
		Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'bannerslider' )->__ ( 'Unable to save Slider Image' ) );
		$this->_redirect ( '*/*/' );
	}
	
	/**
	 * Mass delete action for banners
	 */
	public function massDeleteAction() {
		$bannersliderIds = $this->getRequest ()->getParam ( 'bannerslider' );
		if (! is_array ( $bannersliderIds )) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
		} 
		else {
			try {
				foreach ( $bannersliderIds as $bannersliderId ) {
					$bannerslider = Mage::getModel ( 'bannerslider/bannerslider' )->load ( $bannersliderId );
					$bannerslider->delete ();
				}
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were deleted successfully', count ( $bannersliderIds ) ) );
			} catch ( Exception $e ) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
			}
		}
		$this->_redirect ( '*/*/index' );
	}
	
	/**
	 * Delete action for banner
	 */
	public function deleteAction() {
		if ($this->getRequest ()->getParam ( 'id' ) > 0) {
			try {
				$bannerModel = Mage::getModel ( 'bannerslider/bannerslider' );
				
				$bannerModel->setId ( $this->getRequest ()->getParam ( 'id' ) )->delete ();
				
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Slider Image have been deleted successfully' ) );
				$this->_redirect ( '*/*/' );
			} catch ( Exception $e ) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
				$this->_redirect ( '*/*/edit', array (
						'id' => $this->getRequest ()->getParam ( 'id' ) 
				) );
			}
		}
		$this->_redirect ( '*/*/' );
	}
	
	/**
	 * Mass status update action for banner
	 */
	public function massStatusAction() {
		$bannersliderIds = $this->getRequest ()->getParam ( 'bannerslider' );
		if (! is_array ( $bannersliderIds )) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( $this->__ ( 'Please select item(s)' ) );
		} else {
			try {
				foreach ( $bannersliderIds as $bannersliderId ) {
					$bannerslider = Mage::getSingleton ( 'bannerslider/bannerslider' )->load ( $bannersliderId )->setStatus ( $this->getRequest ()->getParam ( 'status' ) )->save ();
				}
				$this->_getSession ()->addSuccess ( $this->__ ( 'Total of %d record(s) were updated successfully', count ( $bannersliderIds ) ) );
			} catch ( Exception $e ) {
				$this->_getSession ()->addError ( $e->getMessage () );
			}
		}
		$this->_redirect ( '*/*/index' );
	}
}