<?php
class Webkul_ChatSystem_Model_Observer {
    public function chatReminderNotification() {
        $from = Mage::getModel ( 'core/date' )->date ( 'Y-m-d H:i:s' );
        $currentTimestamp = Mage::getModel ( 'core/date' )->timestamp ( time () );
        $lastTime = $currentTimestamp + 3600;
        $to = date ( 'Y-m-d H:i:s', $lastTime );
        $chatReminders = Mage::getModel ( "chatsystem/conversationrequest" )->getCollection ();     
        
        $chatReminders->addFieldToFilter('timeforchat', array(
            'from' => $from,
            'to' => $to,
            'datetime' => true
        ));
        
        $chatReminders->addFieldToFilter ( 'status', array (
            'neq' => 1 
        ) );
        
        foreach ( $chatReminders as $chatReminder ) {
            
            $postObject = new Varien_Object ();
            $postObject->setData ( array (
                'buyeremail' => $chatReminder->getBuyeremail (),
                'buyername' => $chatReminder->getBuyername (),
                'selleremail' => $chatReminder->getSelleremail (),
                'sellername' => $chatReminder->getSellername (),
                'timeforchat' => $chatReminder->getTimeforchat (),
                'productUrl' => $chatReminder->getProducturl () 
            ) );
            
            $translate = Mage::getSingleton ( 'core/translate' );
            $translate->setTranslateInline ( false );
            $sender = array (
                'name' => 'MatexBuyer',
                'email' => 'mtxbuyer@matexnet.com' 
            );
            $storeId = Mage::app ()->getStore ()->getId ();
            $receiverEmail = $chatReminder->getSelleremail ();
            $receiverName = $chatReminder->getSellername ();
            $emailTemplateId = Mage::getStoreConfig ( 'marketplace/buyer_chatrequest_email/chatremind_template' );
            
            $mailTemplate = Mage::getModel ( 'core/email_template' );
            $mailTemplate->getMail ()->addCc ( $chatReminder->getBuyeremail () );
            $mailTemplate->setDesignConfig ( array (
                'area' => 'frontend',
                'store' => Mage::app ()->getStore ()->getId () 
            ) )->sendTransactional ( $emailTemplateId, $sender, $receiverEmail, $receiverName, array (
                'requestchat' => $postObject 
            ) );
            
            $translate->setTranslateInline ( true );
            
            $chatModel = Mage::getModel ( "chatsystem/conversationrequest" )->load ( $chatReminder->getId () );
            $chatModel->setStatus ( 1 );
            $chatModel->save ();
        }
    }
}