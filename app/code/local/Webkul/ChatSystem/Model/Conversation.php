<?php
	class Webkul_ChatSystem_Model_Conversation extends Mage_Core_Model_Abstract	{

    	public function _construct()    {
	        parent::_construct();
	        $this->_init("chatsystem/conversation");
    	}
    	
    	protected function _beforeSave() {
    	    parent::_beforeSave ();
    	    if ($this->isObjectNew ()) {
    	        $this->setData ( 'created_datetime', Varien_Date::now () );
    	    }    	
    	    return $this;
    	}

    }