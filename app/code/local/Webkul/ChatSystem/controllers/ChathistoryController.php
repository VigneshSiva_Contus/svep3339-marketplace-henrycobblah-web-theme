<?php
class Webkul_ChatSystem_ChathistoryController extends Mage_Core_Controller_Front_Action {
    
    /**
     * Pre dispatch action that allows to redirect to no route page in case of disabled extension through admin panel
     */
    public function preDispatch() {
        parent::preDispatch ();
    
        if (! Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->setFlag ( '', 'no-dispatch', true );
            $this->_redirect ( 'noRoute' );
        }
    }
    
    /**
     * Index Action
     */
    public function indexAction() {
        $this->loadLayout ();        
        $this->getLayout ()->getBlock ( 'chathistory' );
        $this->getLayout()->getBlock('head')->setTitle($this->__('Chat History'));
        $this->renderLayout ();
    }
}