<?php
class Webkul_ChatSystem_IndexController extends Mage_Core_Controller_Front_Action	{

    public function saveconversationAction(){
		$wholedata = $this->getRequest()->getParams();		
		$model = Mage::getModel("chatsystem/conversation");
		$model->setFromname($wholedata["fromname"]);
		$model->setForid($wholedata["forid"]);	
		$model->setToname($wholedata["toname"]);
		$model->setToid($wholedata["toid"]);
		$model->setMessage($wholedata["message"]);
		$model->setBuyerId($wholedata["buyerid"]);
		$model->setSellerId($wholedata["sellerid"]);
		$model->setCreatedAt(time());		
		$model->save();
		$admin_notify_coll = Mage::getModel("chatsystem/usernotify")->getCollection()->addFieldToFilter("userid",$wholedata["forid"])->addFieldToFilter("touserid",$wholedata["toid"])->getData();
		if($admin_notify_coll[0]["id"] > 0){
			$model = Mage::getModel("chatsystem/usernotify")->load($admin_notify_coll[0]["id"]);
			$model->setUserid($wholedata["forid"]);
			$model->setTouserid($wholedata["toid"]);
			$model->setStatus(1);
			$model->save();
		}
		else{
			$model = Mage::getModel("chatsystem/usernotify");
			$model->setUserid($wholedata["forid"]);
			$model->setTouserid($wholedata["toid"]);
			$model->setStatus(1);        
			$model->save();
		}			
    }
	
	public function checkifnewusermessagethereAction(){
		$admin_notify_coll = Mage::getModel("chatsystem/usernotify")->getCollection()
		->addFieldToFilter("touserid",Mage::getSingleton('customer/session')->getCustomer()->getId())
		->addFieldToFilter("userid",array("neq"=>0))
		->addFieldToFilter("status",1);
		$arrayName;
		foreach ($admin_notify_coll as $value)  {
			$customer 	= Mage::getModel("customer/customer")->load($value->getUserid());
			$arrayName .= $customer->getName()."-";
			$arrayName .= $value->getUserid().",";
		}
		echo $arrayName;
	}

    public function notifyusermsgisreadnowAction(){
      $admin_notify_coll = Mage::getModel("chatsystem/usernotify")->getCollection()
	  ->addFieldToFilter("userid",$this->getRequest()->getParam("toid"))
	  ->addFieldToFilter("touserid",$this->getRequest()->getParam("userid"))
	  ->getData();
      $model = Mage::getModel("chatsystem/usernotify")->load($admin_notify_coll[0]["id"]);
      $model->setStatus(0);
      $model->save();
    } 

    public function fetchmsgforadminfromusersAction(){
		$wholedata = $this->getrequest()->getParams();
		$collection = Mage::getModel("chatsystem/conversation")->getCollection();
		if($wholedata["toid"]!='admin'){
			$collection->getSelect()->where('(forid ='.$wholedata["id"].' AND toid ='.$wholedata["toid"].') OR (forid ='.$wholedata["toid"].' AND toid ='.$wholedata["id"].')')->order('id DESC')->limit($wholedata["limit"]);
		}
		$fullconversation = array();
		foreach ($collection as $value)
			array_push($fullconversation,array("from" => $value->getFromname(),"message" => $value->getMessage()));
		echo json_encode($fullconversation);
    }
    
    public function requestchatAction() {    	
    	$post = $this->getRequest()->getParams();
    	$cust_id = $post['cust_id'];
    	$seller_id = $post['seller_id'];
    	$selected_date = $post['selected_date'];
    	$selected_chat_topic = $post['chat-topic-select'];
    	$timeforchat = $post['selected_date'];
    	$timeforchat1 = $post['selected_date1'];
    	$product_Id = $post['chatproduct_id'];
    	$customer = Mage::getModel("customer/customer");
    	$seller_email = $customer->load($seller_id)->getEmail();
    	$seller_name = $customer->load($seller_id)->getName();
    	$buyer_email =  $customer->load($cust_id)->getEmail();
    	$buyer_name = $customer->load($cust_id)->getName();
    	$product = Mage::getModel('catalog/product')->load($product_Id);
    	$productName = $product->getName();
    	$productUrl = $product->getProductUrl();
    	$postObject = new Varien_Object();
    	$postObject->setData(array('buyeremail' => $buyer_email,'buyername' => $buyer_name,'selleremail' => $seller_email,'sellername' => $seller_name,'timeforchat' => $timeforchat,  'productUrl' => $productUrl ));    	  
    	
    	$saveData = $postObject->getData();    	
    	$scheduletime = str_replace("/", "-", $saveData['timeforchat']);    	
    	$dateTimestamp = Mage::getModel('core/date')->timestamp(strtotime($scheduletime));
    	$schedule_time = date('Y-m-d H:i:s',strtotime($scheduletime));
    	
    	$model = Mage::getModel("chatsystem/conversationrequest");   	
    	$model->setId(null);
    	$model->setBuyeremail($buyer_email);
    	$model->setBuyername($buyer_name);
    	$model->setSelleremail($seller_email);
    	$model->setSellername($seller_name);
    	$model->setProducturl($productUrl);   	
    	$model->setTimeforchat($schedule_time);
    	$model->setStatus(2);
    	$model->save ();
        try	{   
	    	$translate = Mage::getSingleton('core/translate');
	    	$translate->setTranslateInline(false);
	    	$sender = array('name' => $buyer_name,
	    			'email' => $buyer_email);
	    	$storeId = Mage::app()->getStore()->getId();
	    	$receiverEmail = $seller_email;
	    	$receiverName = $seller_name;    	
	    	$emailTemplateId = Mage::getStoreConfig('marketplace/buyer_chatrequest_email/chatrequest_template');
	    	$mailTemplate = Mage::getModel('core/email_template');
	    	$mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
	    	->sendTransactional(
	    			$emailTemplateId,
	    			$sender,
	    			$receiverEmail,
	    			$receiverName,
	    			array(
	    					'requestchat' => $postObject
	    			)
	    	);	    	
	    	
	    	$translate->setTranslateInline(true);
	    	$message = $this->__('Thank you for your chat request. We will response shortly.');
	    	Mage::getSingleton('core/session')->addSuccess($message);
    	} catch (Exception $e) {
    		$errorMessage =  Mage::getSingleton('core/session')->addError($e->getMessage());
    		Mage::getSingleton('core/session')->addError($errorMessage);
    	}
    	$this->_redirectReferer();
    }
}