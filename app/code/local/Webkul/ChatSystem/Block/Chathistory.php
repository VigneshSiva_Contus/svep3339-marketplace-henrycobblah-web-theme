<?php
/**
 * Groupclone Product
 *
 * Buyer Chat History
 *
 */
class Webkul_ChatSystem_Block_Chathistory extends Mage_Core_Block_Template {
    protected function _prepareLayout() {
        $this->setToolbar ( $this->getLayout ()->createBlock ( 'page/html_pager', 'invitefriends_transaction_toolbar' ) );
        $this->getToolbar ()->setCollection ( $this->_getTransaction () );
    }
    protected function _getCustomer() {
        return Mage::getSingleton ( "customer/session" )->getCustomer ();
    }
    public function _getTransaction() {
        
        $converstions = Mage::getModel ( "chatsystem/conversation" )->getCollection ()->addOrder ( 'created_datetime', 'ASC' );
        
        return $converstions;
    }
    public function getConversations() {
        return $this->getToolbar ()->getCollection ();
    }
    public function getToolbarHtml() {
        return $this->getToolbar ()->toHtml ();
    }
}